import { Injectable } from "@angular/core";
import * as mapboxgl from "mapbox-gl";
import { environment } from "../environments/environment";

@Injectable({
  providedIn: "root"
})
export class MapService {
  map: mapboxgl.Map;
  style = "mapbox://styles/tobias47n9e/ck3eqv20c05re1cqiunr36647";
  lat = 25;
  lng = 0;
  zoom = 12;

  constructor() {}

  buildMap() {
    mapboxgl.accessToken =
      "pk.eyJ1IjoidG9iaWFzNDduOWUiLCJhIjoiY2o0eWtkOXVnMWxxaDMzcW51cngxazF3byJ9.zffmVGsYL_7YpGABooBSrA";
    this.map = new mapboxgl.Map({
      container: "map",
      style: this.style,
      zoom: this.zoom,
      center: [this.lng, this.lat]
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    this.map.on("load", event => {
      this.map.resize();

      this.map.addControl(
        new mapboxgl.GeolocateControl({
          positionOptions: {
            enableHighAccuracy: true
          },
          trackUserLocation: true
        })
      );
    });

    return this.map;
  }
}

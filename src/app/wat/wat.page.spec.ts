import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WatPage } from './wat.page';

describe('WatPage', () => {
  let component: WatPage;
  let fixture: ComponentFixture<WatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

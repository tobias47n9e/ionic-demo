import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WatPage } from './wat.page';

const routes: Routes = [
  {
    path: '',
    component: WatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WatPageRoutingModule {}

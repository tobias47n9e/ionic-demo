import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WatPageRoutingModule } from './wat-routing.module';

import { WatPage } from './wat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WatPageRoutingModule
  ],
  declarations: [WatPage]
})
export class WatPageModule {}

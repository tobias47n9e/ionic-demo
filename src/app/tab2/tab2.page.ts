import { Component, OnInit, AfterViewInit } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import * as mapboxgl from "mapbox-gl";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"]
})
export class Tab2Page implements OnInit, AfterViewInit {
  map: mapboxgl.Map;
  style = "mapbox://styles/tobias47n9e/ck3eqv20c05re1cqiunr36647";
  lat = 47;
  lng = 9;
  zoom = 5;

  constructor(private geolocation: Geolocation) {}

  ngOnInit() {}

  ngAfterViewInit() {
    mapboxgl.accessToken =
      "pk.eyJ1IjoidG9iaWFzNDduOWUiLCJhIjoiY2o0eWtkOXVnMWxxaDMzcW51cngxazF3byJ9.zffmVGsYL_7YpGABooBSrA";

    this.map = new mapboxgl.Map({
      container: "map",
      style: this.style,
      zoom: this.zoom,
      center: [this.lng, this.lat]
    });

    this.map.addControl(new mapboxgl.NavigationControl());

    this.map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );

    this.map.on("load", () => {
      this.map.resize();

      // Add a layer showing the places.
      this.map.addLayer({
        id: "places",
        type: "symbol",
        source: {
          type: "geojson",
          data: {
            type: "FeatureCollection",
            features: [
              {
                type: "Feature",
                properties: {
                  description: "Duck-Marker",
                  icon: "theatre"
                },
                geometry: {
                  type: "Point",
                  coordinates: [9, 47]
                }
              }
            ]
          }
        },
        layout: {
          "icon-image": "{icon}-15",
          "icon-allow-overlap": true
        }
      });

      // When a click event occurs on a feature in the places layer, open a popup at the
      // location of the feature, with description HTML from its properties.
      this.map.on("click", "places", e => {
        var coordinates = e.features[0].geometry.coordinates.slice();
        var description = e.features[0].properties.description;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        new mapboxgl.Popup()
          .setLngLat(coordinates)
          .setHTML(description)
          .addTo(this.map);
      });

      // Change the cursor to a pointer when the mouse is over the places layer.
      this.map.on("mouseenter", "places", () => {
        this.map.getCanvas().style.cursor = "pointer";
      });

      // Change it back to a pointer when it leaves.
      this.map.on("mouseleave", "places", () => {
        this.map.getCanvas().style.cursor = "";
      });
    });
  }
}
